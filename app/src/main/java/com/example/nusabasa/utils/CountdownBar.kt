package com.example.nusabasa.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.ProgressBar

class CountdownBar(context: Context, attrs: AttributeSet?) : ProgressBar(context, attrs) {
    private val progressPaint: Paint = Paint()

    init {
        progressPaint.color = Color.GREEN
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val progressWidth = width.toFloat() * progress / max
        canvas.drawRect(0f, 0f, progressWidth, height.toFloat(), progressPaint)
    }
}
