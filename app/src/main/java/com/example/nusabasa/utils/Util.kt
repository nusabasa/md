package com.example.nusabasa.utils

import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import com.example.nusabasa.R
import com.google.android.material.textfield.TextInputEditText

fun isEmailValid(query: String): Boolean {
    return query.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(query).matches()
}

// A placeholder password validation check
fun isPasswordValid(password: String): Boolean {
    return password.isNotEmpty()
}

fun isNameValid(name: String): Boolean {
    return name.length > 5
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun TextInputEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

data class PlaceholderData(
    val id : Int,
    val placeholderTitle: String,
    val placeholderPhotoUrl: String,
    val placeholderContent: String
)

fun generatePlaceholdersList(size: Int): List<PlaceholderData> {
    val placeholders = mutableListOf<PlaceholderData>()
    for (i in 0 until size) {
        val title = "Title $i"
        val photoUrl = "https://picsum.photos/200"
        val content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut maximus dui at erat euismod molestie. Nullam consectetur risus eu est mattis fermentum."
        val placeholderData = PlaceholderData(i, title, photoUrl, content)
        placeholders.add(placeholderData)
    }
    return placeholders
}

fun generateFunfactList(): List<PlaceholderData>{
    val funfacts = listOf(
        PlaceholderData(
            1,
            "Punya Aksara Sendiri",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki sistem tulisan yang unik dan khas yang dikenal dengan aksara Jawa. Aksara Jawa memiliki sekitar 20 konsonan dan 7 vokal, serta digunakan untuk menulis bahasa Jawa dan juga beberapa bahasa daerah di Jawa."
        ),
        PlaceholderData(
            2,
            "Banyak Dialek",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki banyak dialek yang berbeda di setiap daerahnya. Beberapa dialek yang terkenal antara lain dialek Surakarta, dialek Yogyakarta, dialek Banyumas, dan dialek Tegal. Setiap dialek memiliki ciri khas dan perbedaan dalam pengucapan dan kosakata."
        ),
        PlaceholderData(
            3,
            "Memiliki Ragam Bahasa Formal dan Informal",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki ragam bahasa formal dan informal. Bahasa Jawa formal umumnya digunakan dalam situasi formal, seperti dalam pidato resmi, upacara adat, atau acara resmi lainnya. Sementara itu, bahasa Jawa informal digunakan dalam percakapan sehari-hari dan lingkungan yang lebih santai."
        ),
        PlaceholderData(
            4,
            "Pengaruh Bahasa Sanskerta",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki pengaruh yang kuat dari bahasa Sanskerta. Banyak kata-kata dalam bahasa Jawa yang berasal dari bahasa Sanskerta, terutama dalam konteks agama, sastra, dan kehidupan spiritual. Hal ini menggambarkan sejarah dan pengaruh budaya Hindu-Buddha di Jawa pada masa lalu."
        ),
        PlaceholderData(
            5,
            "Banyak Digunakan dalam Wayang",
            "https://picsum.photos/200",
            "Bahasa Jawa sering digunakan dalam pertunjukan wayang, salah satu bentuk seni tradisional Jawa yang terkenal. Dalang (pengendali boneka wayang) menggunakan bahasa Jawa dalam dialog dan narasi, sehingga bahasa Jawa juga memainkan peran penting dalam melestarikan tradisi wayang."
        ),
        PlaceholderData(
            6,
            "Mengenal Sistem Ngalam",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki sistem khas yang disebut 'ngalam'. Sistem ini digunakan ketika berbicara dengan orang yang lebih tua, orang yang dihormati, atau dalam situasi formal. Dalam sistem ngalam, kata ganti orang pertama 'aku' diganti dengan 'kula', dan kata ganti orang kedua 'engkau' diganti dengan 'kowe'."
        ),
        PlaceholderData(
            7,
            "Ada Bentuk Krama Inggil",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki bentuk khusus yang sangat hormat yang disebut 'krama inggil'. Bentuk ini digunakan ketika berbicara dengan orang yang sangat dihormati atau dalam situasi yang sangat resmi. Penggunaan krama inggil menunjukkan adab, sopan santun, dan penghormatan yang tinggi."
        ),
        PlaceholderData(
            8,
            "Pengaruh Bahasa Jawa dalam Bahasa Indonesia",
            "https://picsum.photos/200",
            "Bahasa Jawa telah memberikan kontribusi yang signifikan dalam perkembangan bahasa Indonesia. Banyak kata-kata dalam bahasa Indonesia yang berasal dari bahasa Jawa. Misalnya, kata 'sudah', 'malam', 'siang', dan 'minggu' adalah beberapa kata serapan dari bahasa Jawa yang umum digunakan dalam bahasa Indonesia sehari-hari."
        ),
        PlaceholderData(
            9,
            "Dikenal dengan Pepatah dan Peribahasa",
            "https://picsum.photos/200",
            "Bahasa Jawa kaya dengan pepatah dan peribahasa yang mengandung nilai-nilai budaya, kebijaksanaan, dan hikmah. Pepatah dan peribahasa ini sering digunakan dalam percakapan sehari-hari untuk menyampaikan pesan dan memberikan nasihat. Contoh populer adalah 'Ora et labora' yang berarti 'Berdoa dan bekerja'."
        ),
        PlaceholderData(
            10,
            "Mempunyai Ciri Khas Bunyi Konsonan",
            "https://picsum.photos/200",
            "Bahasa Jawa memiliki ciri khas dalam bunyi konsonan. Beberapa konsonan dalam bahasa Jawa memiliki variasi yang lebih banyak dibandingkan dengan bahasa Indonesia. Misalnya, bunyi /c/ dalam bahasa Jawa dapat bervariasi menjadi /c/, /ch/, atau /ts/, tergantung pada dialek dan konteksnya."
        )
    )

    return funfacts
}

private val initialMessageList = listOf(
    "Selamat Pagi Nusa",
    "Selamat Siang Nusa",
    "Selamat Sore Nusa",
    "Rek Nusa",
    "Halo Nusa",
    "Hi Nusa",
    "Apa kabar hari ini?",
    "Ini aplikasi apa Nusa?",
    "Aplikasi Nusabasa itu apa?",
    "Jelaskan proses belajar",
    "XP itu apa?",
    "Poin itu apa?",
    "Apa itu halaman pencapaian?",
    "Jelaskan halaman leaderboard",
    "Cara mengubah profil",
    "Bagaimana cara ubah profil?",
    "Apakah harus buat akun?",
    "Apa harus login?",
    "Kasih aku satu lelucon",
    "Beri aku lelucon",
    "Fakta tentang Jawa",
    "Trivia tentang jawa!",
    "Terima kasih Nusa!",
    "Makasih ya Nusa!"
)

fun getRandomMessages(numRandom: Int): List<String> {
    val shuffledList = initialMessageList.shuffled()
    return shuffledList.take(numRandom).distinct().take(numRandom)
}

fun getRandomDrawables(numDrawables: Int): List<Int> {
    val randomDrawable = listOf(
        R.drawable.img_quiz_1,
        R.drawable.img_quiz_2,
        R.drawable.img_quiz_3,
        R.drawable.img_quiz_4,
        R.drawable.img_quiz_5,
        R.drawable.img_quiz_6,
        R.drawable.img_quiz_7,
        R.drawable.img_quiz_8,
        R.drawable.img_quiz_9,
        R.drawable.img_quiz_10,
    )

    val shuffledList = randomDrawable.shuffled()
    return shuffledList.take(numDrawables).distinct().take(numDrawables)

}

