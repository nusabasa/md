package com.example.nusabasa.data.api


import com.example.nusabasa.data.api.request.ChatBotRequest
import com.example.nusabasa.data.api.request.LoginRequest
import com.example.nusabasa.data.api.request.QuizSubmitRequest
import com.example.nusabasa.data.api.request.RegisterRequest
import com.example.nusabasa.data.api.response.ChatBotResponse
import com.example.nusabasa.data.api.response.LoginResponse
import com.example.nusabasa.data.api.response.QuizSubmitResponse
import com.example.nusabasa.data.api.response.RegisterResponse
import com.google.gson.JsonElement
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @POST("/users/register")
    suspend fun register(
        @Body request: RegisterRequest
    ): Response<RegisterResponse>

    @POST("/users/login")
    suspend fun login(
        @Body request: LoginRequest
    ): Response<LoginResponse>

    @POST("/chatbot")
    suspend fun sendChatMessage(
        @Body request: ChatBotRequest,
        @Header("Authorization") token: String
    ): Response<ChatBotResponse>

    @GET("/categories")
    suspend fun getMaterialCategory(): Response<JsonElement>

    @GET("/users/leaderboard")
    suspend fun getLeaderboard(
        @Header("Authorization") token: String
    ): Response<JsonElement>

    @GET("/quiz/{categoryId}/{levelId}")
    suspend fun getQuiz(
        @Header("Authorization") token: String,
        @Path("categoryId") categoryId: Int,
        @Path("levelId") levelId: Int
    ): Response<JsonElement>

    @POST("/quiz/submitQuiz")
    suspend fun submitQuiz(
        @Header("Authorization") token: String,
        @Body request: QuizSubmitRequest
    ) : Response<QuizSubmitResponse>

    @GET("/material/get")
    suspend fun getSpecifiedMaterial(
        @Query("category") categoryId: Int,
        @Query("level") levelId: Int
    ): Response<JsonElement>



}