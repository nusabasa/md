package com.example.nusabasa.data.api.request

data class QuizSubmitRequest(
    val point: Int
)