package com.example.nusabasa.data.api.repository

import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.datasource.QuizDataSource
import com.example.nusabasa.data.api.response.QuizResponse

class QuizRepository(private val quizDataSource: QuizDataSource) {
    suspend fun getQuiz(categoryId: Int, levelId: Int, token:String) : Result<QuizResponse> {
        return quizDataSource.getQuiz(categoryId, levelId, token)
    }

    suspend fun submitQuiz(point: Int, token: String) = quizDataSource.submitQuiz(point, token)
}