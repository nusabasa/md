package com.example.nusabasa.data.api.response

import com.google.gson.annotations.SerializedName

data class MaterialCategoryResponse(
	@field:SerializedName("MaterialCategoryResponse")
	val materialCategoryResponse: List<MaterialCategoryResponseItem?>? = null
)
data class MaterialCategoryResponseItem(

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
