package com.example.nusabasa.data.api.repository

import android.util.Log
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.request.ChatBotRequest
import com.example.nusabasa.data.api.ApiConfig
import com.example.nusabasa.ui.main.chatbot.Message
import com.example.nusabasa.ui.main.chatbot.MessageType

class ChatBotRepository {

    private val chatBotService = ApiConfig.getApiService()

    suspend fun sendMessage(message: String, token: String): Result<Message> {
        return try {
            val jwtToken = "Bearer $token"
            val response = chatBotService.sendChatMessage(
                request = ChatBotRequest(message),
                token = jwtToken
            )
            if (response.isSuccessful) {
                val receivedMessage = Message(
                    MessageType.RECEIVED_MESSAGE,
                    response.body()?.response.toString()
                )
                Log.d(TAG, "RESPONSE: $receivedMessage TOKEN: $jwtToken")
                Result.Success(receivedMessage)
            } else {
                Log.e(TAG, "RESPONSE ERROR: ${response.code()}, TOKEN: $jwtToken")
                Result.Error("Oops! Ada masalah :(")
            }
        } catch (e: Exception) {
            Log.e(TAG, "Oops! Ada masalah :(: ${e.message}")
            Result.Error("Oops! Ada masalah :(.")
        }
    }


    companion object {
        private const val TAG = "ChatBotRepository"
    }

}