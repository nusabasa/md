package com.example.nusabasa.data.api.datasource

import android.util.Log
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.ApiService
import com.example.nusabasa.data.api.response.LeaderboardResponse
import com.example.nusabasa.data.api.response.LeaderboardResponseItem
import com.example.nusabasa.data.api.response.MaterialCategoryResponse
import com.example.nusabasa.data.api.response.MaterialCategoryResponseItem
import com.google.gson.Gson

class LeaderboardDataSource(private val apiService: ApiService) {

    suspend fun getLeaderboard(token: String) : Result<LeaderboardResponse> {
        val response = apiService.getLeaderboard(
            token = "Bearer $token"
        )

        val json = response.body()

        return if(response.isSuccessful && (json != null) && json.isJsonArray){
            val leaderboardResponseItem = Gson().fromJson(json, Array<LeaderboardResponseItem>::class.java)

            val result = LeaderboardResponse(ArrayList(leaderboardResponseItem.asList()))
            Log.d(TAG, "RESPOSNE GOOD : $result")
            Result.Success(result)
        } else {
            Result.Error("FAILED")
        }

    }

    companion object {
        private const val TAG = "LeaderboardDataSource"
    }

}