package com.example.nusabasa.data.di

import android.content.Context
import com.example.nusabasa.data.api.ApiConfig
import com.example.nusabasa.data.api.datasource.AuthDataSource
import com.example.nusabasa.data.api.datasource.LeaderboardDataSource
import com.example.nusabasa.data.api.datasource.MaterialDataSource
import com.example.nusabasa.data.api.datasource.QuizDataSource
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.api.repository.ChatBotRepository
import com.example.nusabasa.data.api.repository.LeaderboardRepository
import com.example.nusabasa.data.api.repository.MaterialRepository
import com.example.nusabasa.data.api.repository.QuizRepository

object Injection {

    private val apiService = ApiConfig.getApiService()

    private val materialDataSource = MaterialDataSource(apiService)
    private val leaderboardDataSource = LeaderboardDataSource(apiService)
    private val quizDataSource = QuizDataSource(apiService)

    private val authDataSource = AuthDataSource()

    fun provideMaterialRepository(): MaterialRepository {
        return MaterialRepository(
            materialDataSource
        )
    }

    fun provideLeaderboardRepository(): LeaderboardRepository{
        return LeaderboardRepository(leaderboardDataSource)
    }

    fun provideQuizRepository(): QuizRepository{
        return QuizRepository(quizDataSource)
    }

    fun provideAuthRepository(context: Context): AuthRepository {
        return AuthRepository(authDataSource, context)
    }

    fun provideChatBotRepository(): ChatBotRepository {
        return ChatBotRepository()
    }
}