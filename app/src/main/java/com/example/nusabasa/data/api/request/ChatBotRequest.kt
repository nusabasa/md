package com.example.nusabasa.data.api.request

data class ChatBotRequest (
    val message: String?
)