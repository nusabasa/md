package com.example.nusabasa.data.model

/**
 * Data class that captures user information for logged in users retrieved from AuthRepository
 */
data class LoggedInUser(
    val userId: Int?,
    val name: String?,
    val token: String?,
    val email: String?
)