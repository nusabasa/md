package com.example.nusabasa.data.api.datasource

import android.util.Log
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.ApiConfig
import com.example.nusabasa.data.api.request.LoginRequest
import com.example.nusabasa.data.api.request.RegisterRequest
import com.example.nusabasa.data.api.response.LoginResponse
import com.example.nusabasa.data.api.response.RegisterResponse
import com.example.nusabasa.data.model.LoggedInUser
import com.example.nusabasa.data.model.RegisteredUser
import com.example.nusabasa.utils.isEmailValid
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response


/**
 * Class that handles authentication w/ auth credentials and retrieves user information.
 */
class AuthDataSource {

    private val apiService = ApiConfig.getApiService()

    suspend fun login(identifier: String, password: String): Result<LoggedInUser> {
        return if(isEmailValid(identifier)){
            val requestBody = LoginRequest(
                username= null,
                email = identifier,
                password = password
            )
            handleLoginResponse(apiService.login(requestBody))
        } else {
            val requestBody = LoginRequest(
                username = identifier,
                email = null,
                password = password
            )
            handleLoginResponse(apiService.login(requestBody))
        }
    }


    private fun handleLoginResponse(response: Response<LoginResponse>): Result<LoggedInUser> {
        return if (response.isSuccessful){
            val loggedInUser = LoggedInUser(
                userId = response.body()?.userId,
                name = response.body()?.username,
                token = response.body()?.token,
                email = response.body()?.email
            )

            Log.d(TAG, "onResponse LOGIN Success in $TAG, $loggedInUser")
            Result.Success(loggedInUser)
        } else {
            val errorMessage = response.errorBody()?.string()?.let {errorBody ->
                try {
                    JSONObject(errorBody).getString("message")
                } catch (e: JSONException){
                    "Unknown error"
                }
            } ?: "Unknown error"

            Log.e(TAG, "onResponse Error in $TAG ${response.code()} $errorMessage")

            Result.Error("Login Gagal :  $errorMessage")
        }
    }

    suspend fun register(name: String, email: String, password: String): Result<RegisteredUser> {
        val requestBody = RegisterRequest(name, email, password)
        val response = apiService.register(requestBody)
        return handleRegisterResponse(response)
    }

    private fun handleRegisterResponse(response: Response<RegisterResponse>): Result<RegisteredUser> {
        return if (response.isSuccessful) {
            val registeredUser = RegisteredUser(response.body()?.error, response.body()?.message)
            Log.d(TAG, "onResponse REGISTER Success in $TAG, ${response.body()?.message}")
            Result.Success(registeredUser)
        } else {
            val errorMessage = response.errorBody()?.string()?.let {errorBody ->
                try {
                    JSONObject(errorBody).getString("message")
                } catch (e: JSONException) {
                    "Unknown error"
                }
            } ?: "Unknown error"
            Log.e(TAG, "onResponse Error in $TAG ${response.code()} $errorMessage")

            Result.Error("Pendaftaran Akun Gagal :  $errorMessage")
        }
    }

    companion object{
        private const val TAG = "AuthDataSource"
    }
}