package com.example.nusabasa.data.api.repository

import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.datasource.LeaderboardDataSource
import com.example.nusabasa.data.api.response.LeaderboardResponse

class LeaderboardRepository(private val leaderboardDataSource: LeaderboardDataSource) {

    suspend fun getLeaderboard(token: String):  Result<LeaderboardResponse> {
        return leaderboardDataSource.getLeaderboard(token)
    }

}