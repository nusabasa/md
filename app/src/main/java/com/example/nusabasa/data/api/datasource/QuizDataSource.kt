package com.example.nusabasa.data.api.datasource

import android.util.Log
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.ApiService
import com.example.nusabasa.data.api.request.QuizSubmitRequest
import com.example.nusabasa.data.api.response.MaterialCategoryResponse
import com.example.nusabasa.data.api.response.MaterialCategoryResponseItem
import com.example.nusabasa.data.api.response.QuizResponse
import com.example.nusabasa.data.api.response.QuizResponseItem
import com.example.nusabasa.data.api.response.QuizSubmitResponse
import com.google.gson.Gson

class QuizDataSource(private val apiService: ApiService) {

    suspend fun getQuiz(categoryId: Int, levelId: Int, token: String) : Result<QuizResponse>{
        val response = apiService.getQuiz(token = token, categoryId = categoryId, levelId = levelId)

        val json = response.body()

        return if(response.isSuccessful && (json != null) && json.isJsonArray){
            val materialCategoryResponseItem = Gson().fromJson(json, Array<QuizResponseItem>::class.java)

            val result = QuizResponse(ArrayList(materialCategoryResponseItem.asList()))
            Log.d(TAG, "RESPOSNE GOOD : $result")
            Result.Success(result)
        } else {
            Result.Error("FAILED")
        }
    }

    suspend fun submitQuiz(point: Int, token: String) : Result<QuizSubmitResponse> {
        val response = apiService.submitQuiz(token, request = QuizSubmitRequest(point))
        return try {
            if (response.isSuccessful){
                Result.Success(QuizSubmitResponse("XP Berhasil Ditambahkan!"))
            } else {
                Result.Error("Maaf! Ada Kesalahan")
            }
        } catch (e: Exception){
            Result.Error("Maaf! Ada Kesalahan")
        }
    }

    companion object {
        private const val TAG = "QuizDataSource"
    }

}