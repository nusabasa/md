package com.example.nusabasa.data.api.response

import com.google.gson.annotations.SerializedName

data class SpecifiedMaterialResponseItem(

	@field:SerializedName("CategoryId")
	val categoryId: Int? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("LevelId")
	val levelId: Int? = null,

	@field:SerializedName("lessons")
	val lessons: List<LessonsItem?>? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)

data class LessonsItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: List<String?>? = null
)

data class SpecifiedMaterialResponse(
	@field:SerializedName("specifiedMaterialResponse")
	val specifiedMaterialResponse: List<SpecifiedMaterialResponseItem?>? = null
)
