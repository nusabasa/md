package com.example.nusabasa.data.api.request

data class RegisterRequest(
    val username: String,
    val email: String,
    val password: String
)