package com.example.nusabasa.data.api.repository

import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.datasource.MaterialDataSource
import com.example.nusabasa.data.api.response.MaterialCategoryResponse

class MaterialRepository(private val materialDataSource: MaterialDataSource) {

    suspend fun getMaterialCategory(): Result<MaterialCategoryResponse> {
        return materialDataSource.getMaterialCategory()
    }

    suspend fun getSpecifiedMaterial(categoryId: Int, levelId: Int) =
        materialDataSource.getSpecifiedMaterial(categoryId, levelId)

}