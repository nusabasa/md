package com.example.nusabasa.data.api.response

import com.google.gson.annotations.SerializedName

data class QuizSubmitResponse(

	@field:SerializedName("success")
	val success: String? = null
)
