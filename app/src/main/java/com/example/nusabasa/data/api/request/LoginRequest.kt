package com.example.nusabasa.data.api.request

data class LoginRequest(
    val username: String?,
    val email: String?,
    val password: String
)