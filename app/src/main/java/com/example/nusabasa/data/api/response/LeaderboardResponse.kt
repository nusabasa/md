package com.example.nusabasa.data.api.response

import com.google.gson.annotations.SerializedName

data class LeaderboardResponse(
	@field:SerializedName("LeaderboardResponse")
	val leaderboardResponse: List<LeaderboardResponseItem?>? = null
)

data class LeaderboardResponseItem(

	@field:SerializedName("username")
	val username: String? = null,

	@field:SerializedName("userpoint")
	val userpoint: Int? = null
)
