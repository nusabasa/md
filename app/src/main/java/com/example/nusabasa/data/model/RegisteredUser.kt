package com.example.nusabasa.data.model

data class RegisteredUser(
    val error: Boolean?,
    val message: String?
)

