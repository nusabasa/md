package com.example.nusabasa.data.api.response

import com.google.gson.annotations.SerializedName

data class QuizResponse(
	@field:SerializedName("QuizResponse")
	val quizResponse: List<QuizResponseItem?>? = null
)
data class QuizResponseItem(

	@field:SerializedName("CategoryId")
	val categoryId: Int? = null,

	@field:SerializedName("a")
	val a: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("b")
	val b: String? = null,

	@field:SerializedName("c")
	val c: String? = null,

	@field:SerializedName("d")
	val d: String? = null,

	@field:SerializedName("question")
	val question: String? = null,

	@field:SerializedName("answer")
	val answer: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("LevelId")
	val levelId: Int? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null
)
