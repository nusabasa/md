package com.example.nusabasa.data.api.repository

import android.util.Log
import android.content.Context
import android.content.SharedPreferences
import com.example.nusabasa.data.api.datasource.AuthDataSource
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.model.LoggedInUser
import com.example.nusabasa.data.model.RegisteredUser


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of auth status and user credentials information.
 */

class AuthRepository(private val dataSource: AuthDataSource, val context: Context) {

    private var sharedPrefs: SharedPreferences =
        context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)

    fun getUsername(): String? {
        return sharedPrefs.getString(USERNAME_KEY, null)
    }

    fun getEmail(): String? {
        return sharedPrefs.getString(EMAIL_KEY, null)
    }

    fun getToken(): String? {
        return sharedPrefs.getString(TOKEN_KEY, null)
    }

    fun getUserId(): String? {
        return sharedPrefs.getString(USERID_KEY, null)
    }

    fun logout() {
        sharedPrefs.edit().remove(TOKEN_KEY).apply()
    }

    suspend fun login(username: String, password: String): Result<LoggedInUser> {
        // handle login
        val result = dataSource.login(username, password)

        Log.d(TAG, "Login Function in $TAG Called, Result is $result")

        if (result is Result.Success) {
            setLoggedInUser(result.data.userId, result.data.name, result.data.token, result.data.email)
        }

        return result
    }

    suspend fun register(name: String, email: String, password: String): Result<RegisteredUser> {
        //Call data Source
        val result = dataSource.register(name, email, password)

        Log.d(TAG, "Register Function in $TAG Called, Result is $result")

        return result
    }

    private fun setLoggedInUser(userId: Int?, name: String?, token: String?, email: String?) {
        // Store the token in SharedPreferences
        with (sharedPrefs.edit()) {
            putString(USERID_KEY, userId.toString())
            putString(TOKEN_KEY, token)
            putString(USERNAME_KEY, name)
            putString(EMAIL_KEY, email)
            apply()
        }

        Log.d(TAG, "TOKEN : $token")
    }

    companion object{
        private const val USERID_KEY = "user_id"
        private const val TAG = "AuthRepository"
        private const val SHARED_PREFS_NAME = "MySharedPrefs"
        private const val TOKEN_KEY = "token"
        private const val USERNAME_KEY = "username"
        private const val EMAIL_KEY = "email"
    }
}