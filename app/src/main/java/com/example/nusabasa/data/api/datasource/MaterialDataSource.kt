package com.example.nusabasa.data.api.datasource

import android.util.Log
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.ApiService
import com.example.nusabasa.data.api.response.MaterialCategoryResponse
import com.example.nusabasa.data.api.response.MaterialCategoryResponseItem
import com.example.nusabasa.data.api.response.SpecifiedMaterialResponse
import com.example.nusabasa.data.api.response.SpecifiedMaterialResponseItem
import com.google.gson.Gson

class MaterialDataSource(private val apiService: ApiService) {

    suspend fun getMaterialCategory(): Result<MaterialCategoryResponse>{
        val response = apiService.getMaterialCategory()

        val json = response.body()
        Log.d(TAG, "RESPOSNE GOOD : $json")

        return if(response.isSuccessful && (json != null) && json.isJsonArray){
            val materialCategoryResponseItem = Gson().fromJson(json, Array<MaterialCategoryResponseItem>::class.java)

            val result = MaterialCategoryResponse(ArrayList(materialCategoryResponseItem.asList()))
            Log.d(TAG, "RESPOSNE GOOD : $result")
            Result.Success(result)
        } else {
            Result.Error("FAILED")
        }
    }

    suspend fun getSpecifiedMaterial(categoryId: Int, levelId: Int): Result<SpecifiedMaterialResponse> {
        val response = apiService.getSpecifiedMaterial(categoryId, levelId)

        val json = response.body()
        Log.d(TAG, "RESPONSE: $json")

        return if (response.isSuccessful && json != null && json.isJsonArray) {
            val specifiedMaterialResponseItem =
                Gson().fromJson(json, Array<SpecifiedMaterialResponseItem>::class.java)

            val result = SpecifiedMaterialResponse(ArrayList(specifiedMaterialResponseItem.asList()))
            Log.d(TAG, "RESPONSE: $result")
            Result.Success(result)
        } else {
            Result.Error("Failed to fetch specified material")
        }
    }


    companion object {
        private const val TAG = "MaterialDataSource"
    }

}