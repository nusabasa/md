package com.example.nusabasa.data.api.response

import com.google.gson.annotations.SerializedName

data class ChatBotResponse(
	@field:SerializedName("response")
	val response: String? = null,
)
