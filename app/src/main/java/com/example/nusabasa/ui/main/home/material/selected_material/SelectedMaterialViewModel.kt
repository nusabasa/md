package com.example.nusabasa.ui.main.home.material.selected_material

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.repository.MaterialRepository
import com.example.nusabasa.data.api.response.SpecifiedMaterialResponseItem
import kotlinx.coroutines.launch

class SelectedMaterialViewModel(
    private val materialRepository: MaterialRepository

) : ViewModel() {
    private val _specifiedMaterialList = MutableLiveData<SpecifiedMaterialResponseItem>()
    val specifiedMaterialList : LiveData<SpecifiedMaterialResponseItem> = _specifiedMaterialList

    private val _specifiedMaterialLessonCount = MutableLiveData<Int>()
    val specifiedMaterialLessonCount: LiveData<Int> = _specifiedMaterialLessonCount

    fun getSpecifiedMaterial(categoryId: Int, levelId: Int) {

        viewModelScope.launch {

            val result = materialRepository.getSpecifiedMaterial(categoryId, levelId)

            if (result is Result.Success){
                _specifiedMaterialList.value = result.data.specifiedMaterialResponse?.get(0)

                val materials = _specifiedMaterialList.value

                _specifiedMaterialLessonCount.value = materials?.lessons?.size

                Log.d(TAG, "getSpecifiedMaterial: ${result.data.specifiedMaterialResponse}")
            } else {
                Log.d(TAG, "getSpecifiedMaterial: ${result}")
            }
        }
    }



    companion object {
        private const val TAG = "SelectedMaterialViewModel"
    }

}