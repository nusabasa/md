package com.example.nusabasa.ui.main.home.material.quiz.misc

enum class ButtonState {
    CLICK_TOAST,
    CLICK_NEXT_QUESTION
}
