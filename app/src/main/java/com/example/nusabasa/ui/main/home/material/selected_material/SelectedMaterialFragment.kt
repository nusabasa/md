package com.example.nusabasa.ui.main.home.material.selected_material

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.nusabasa.data.api.response.LessonsItem
import com.example.nusabasa.databinding.FragmentSelectedMaterialBinding
import com.example.nusabasa.ui.main.MainViewModelFactory
import com.example.nusabasa.ui.main.home.HomeFragment
import com.example.nusabasa.ui.main.home.HomeFragment.Companion.CATEGORY_NAME
import com.example.nusabasa.ui.main.home.material.MaterialFragment

class SelectedMaterialFragment : Fragment() {

    private val selectedMaterialViewModel by viewModels<SelectedMaterialViewModel> {
        MainViewModelFactory(requireContext())
    }

    private var _binding: FragmentSelectedMaterialBinding? = null
    private val binding get() = _binding!!

    private var currentPageIndex = 0
    private var categoryDifficulty: String? = null
    private var categoryName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSelectedMaterialBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryDifficulty = arguments?.getString(MaterialFragment.MATERIAL_CATEGORY_DIFFICULTY_NAME)
        val categoryId = arguments?.getInt(HomeFragment.CATEGORY_ID)
        val levelId = arguments?.getInt(MaterialFragment.LEVEL_ID)

        categoryName = arguments?.getString(CATEGORY_NAME)

        selectedMaterialViewModel.getSpecifiedMaterial(categoryId!!, levelId!!)

        selectedMaterialViewModel.specifiedMaterialList.observe(viewLifecycleOwner, Observer{
            val specifiedMaterialList = it ?: return@Observer

            showLessonData(specifiedMaterialList.lessons?.get(currentPageIndex))

            binding.previousButton.setOnClickListener {
                currentPageIndex--
                showLessonData(specifiedMaterialList.lessons?.get(currentPageIndex))
            }

            binding.nextButton.setOnClickListener {
                currentPageIndex++
                navigateToNextPage(specifiedMaterialList.lessons?.get(currentPageIndex))
            }
        })
    }

    private fun showLessonData(lesson: LessonsItem?) {
        binding.materialTitle.text = lesson?.title ?: ""
        binding.materialContent.text = lesson?.content?.joinToString("\n") ?: ""
        binding.subtitle.text = categoryDifficulty
        binding.categoryindicator.text = categoryName
        updateButtonState(currentPageIndex)
    }

    private fun navigateToNextPage(specifiedMaterialResponseItem: LessonsItem?) {
        if (currentPageIndex < getLessonCount()) {
            showLessonData(specifiedMaterialResponseItem)
        } else {
            Toast.makeText(requireContext(), "End Reached", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateButtonState(index: Int) {
        val isFirstPage = index == 0
        val isLastPage = index == getLessonCount() - 1

        Log.d(TAG, "updateButtonState: ${getLessonCount()}")
        Log.d(TAG, "updateButtonState: ${isFirstPage}")
        Log.d(TAG, "updateButtonState: ${isLastPage}")

        binding.previousButton.isEnabled = !isFirstPage
        binding.nextButton.isEnabled = !isLastPage
    }

    private fun getLessonCount(): Int {
        return selectedMaterialViewModel.specifiedMaterialList.value!!.lessons!!.size
    }

    companion object {
        private const val TAG = "SelectedMaterialFragment"
        const val MATERIAL_CATEGORY = "material_category"
        const val PROGRESS_INDEX = "progress_index"

        fun newInstance(category: String): SelectedMaterialFragment {
            val args = Bundle().apply {
                putString(MATERIAL_CATEGORY, category)
            }
            val fragment = SelectedMaterialFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}



