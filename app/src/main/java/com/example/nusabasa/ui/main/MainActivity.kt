package com.example.nusabasa.ui.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.nusabasa.R
import com.example.nusabasa.databinding.ActivityMainBinding
import com.example.nusabasa.ui.main.chatbot.ChatBotActivity
import com.example.nusabasa.ui.misc.NetworkConnectivityListener

class MainActivity : AppCompatActivity(), NetworkConnectivityListener.NetworkListener {

    private lateinit var binding : ActivityMainBinding
    private lateinit var networkConnectivityListener: NetworkConnectivityListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        if (!allPermissionsGranted()) {
            ActivityCompat.requestPermissions(
                this,
                REQUIRED_PERMISSION,
                REQUEST_CODE
            )
        }

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val bottomNavView = binding.bottomNavigationView

        val navController = findNavController(R.id.nav_host_fragment_activity_main_page)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        val bottomNavDestinations = setOf(
            R.id.navigation_home,
            R.id.navigation_achievement,
            R.id.navigation_profile
        )

        val secondaryDestinations = setOf(
            R.id.materialFragment2,
            R.id.selectedMaterialFragment,
            R.id.navigation_achievement,
            R.id.navigation_profile
        )

        val appBarConfiguration = AppBarConfiguration(
            bottomNavDestinations
        )

        binding.toolbar.setNavigationOnClickListener {
            navController.navigateUp()
        }

        binding.toolbar.setTitleTextColor(getColor(R.color.white))
        binding.toolbar.navigationIcon?.setColorFilter(getColor(R.color.white), PorterDuff.Mode.SRC_IN)

        //Visibility Controller
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id in secondaryDestinations ) {
                binding.toolbar.visibility = View.VISIBLE
            } else {
                binding.toolbar.visibility = View.GONE
            }

            if (destination.id in bottomNavDestinations){
                binding.cvBottomNavContainer.visibility = View.VISIBLE
                binding.fabChatbot.visibility = View.VISIBLE
            } else {
                binding.fabChatbot.visibility = View.GONE
                binding.cvBottomNavContainer.visibility = View.GONE
            }
        }

        binding.fabChatbot.setOnClickListener {
            startActivity(Intent(this, ChatBotActivity::class.java))

        }

        setupActionBarWithNavController(navController, appBarConfiguration)
        bottomNavView.setupWithNavController(navController)

        networkConnectivityListener = NetworkConnectivityListener(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d(TAG, "onRequestPermissionsResult: Permission $requestCode")

        if (requestCode == REQUEST_CODE){
            if (!allPermissionsGranted()) {
                Log.d(TAG, "onRequestPermissionsResult: Permission not granted")
            }
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSION.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onResume() {
        super.onResume()
        registerNetworkReceiver()
    }

    override fun onPause() {
        super.onPause()
        unregisterNetworkReceiver()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterNetworkReceiver()
    }

    private fun registerNetworkReceiver() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkConnectivityListener, intentFilter)
    }

    private fun unregisterNetworkReceiver() {
        try {
            unregisterReceiver(networkConnectivityListener)
        } catch (e: IllegalArgumentException) {
            // Receiver was not registered, ignore the exception
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            showNetworkDialog()
        } else {
            // Connection is restored, perform necessary actions here
        }
    }

    private fun showNetworkDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Maaf... 😔")
            .setMessage("Nusabasa tidak bisa berjalan dengan optimal tanpa koneksi internet. Periksa kembali jaringan kamu")
            .setPositiveButton("Coba Lagi") { dialog, _ ->
                // Retry logic
                dialog.dismiss()
                retryConnection()
            }
            .setNegativeButton("Keluar Aplikasi") { dialog, _ ->
                // User clicked "No" button, dismiss the dialog and finish the activity
                dialog.dismiss()
                finish()
            }
            .setCancelable(false) // Prevent dialog dismissal when clicking outside or pressing back button
            .setIcon(R.drawable.ic_wifi_low)

        val dialog = builder.create()
        dialog.show()
    }

    private fun retryConnection() {
        // Perform the necessary actions to check the connection again
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if (isConnected) {
            // Connection is restored, perform necessary actions here
        } else {
            showNetworkDialog()
        }
    }

    companion object {
        private const val TAG = "MainActivity"
        private val REQUIRED_PERMISSION = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.MANAGE_EXTERNAL_STORAGE)
        private const val REQUEST_CODE = 110
    }
}