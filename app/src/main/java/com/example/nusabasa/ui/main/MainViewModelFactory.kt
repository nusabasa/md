package com.example.nusabasa.ui.main

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nusabasa.data.di.Injection
import com.example.nusabasa.ui.main.achievement.AchievementViewModel
import com.example.nusabasa.ui.main.chatbot.ChatBotViewModel
import com.example.nusabasa.ui.main.home.HomeViewModel
import com.example.nusabasa.ui.main.home.material.quiz.result.QuizResultDialogViewModel
import com.example.nusabasa.ui.main.home.material.quiz.QuizViewModel
import com.example.nusabasa.ui.main.home.material.selected_material.SelectedMaterialViewModel
import com.example.nusabasa.ui.main.profile.ProfileViewModel

class MainViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(
                materialRepository = Injection.provideMaterialRepository(),
                authRepository = Injection.provideAuthRepository(context)
            ) as T
        }

        if (modelClass.isAssignableFrom(AchievementViewModel::class.java)){
            return AchievementViewModel(
                leaderboardRepository = Injection.provideLeaderboardRepository(),
                authRepository = Injection.provideAuthRepository(context)
            ) as T
        }

        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)){
            return ProfileViewModel(Injection.provideAuthRepository(context)) as T
        }

        if (modelClass.isAssignableFrom(ChatBotViewModel::class.java)){
            return ChatBotViewModel(
                authRepository = Injection.provideAuthRepository(context),
                chatBotRepository = Injection.provideChatBotRepository()
            ) as T
        }

        if (modelClass.isAssignableFrom(QuizViewModel::class.java)){
            return QuizViewModel(
                authRepository = Injection.provideAuthRepository(context),
                quizRepository = Injection.provideQuizRepository()
            ) as T
        }

        if (modelClass.isAssignableFrom(QuizResultDialogViewModel::class.java)){
            return QuizResultDialogViewModel(
                authRepository = Injection.provideAuthRepository(context),
                quizRepository = Injection.provideQuizRepository()
            ) as T
        }

        if (modelClass.isAssignableFrom(SelectedMaterialViewModel::class.java)){
            return SelectedMaterialViewModel(
                materialRepository = Injection.provideMaterialRepository()
            ) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}