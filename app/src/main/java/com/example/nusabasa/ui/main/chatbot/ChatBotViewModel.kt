package com.example.nusabasa.ui.main.chatbot

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.api.repository.ChatBotRepository
import kotlinx.coroutines.launch

class ChatBotViewModel(
    private val authRepository: AuthRepository,
    private val chatBotRepository: ChatBotRepository
): ViewModel() {

    private val _messageList = MutableLiveData<List<Message>>()
    val messageList: LiveData<List<Message>> = _messageList

    private val _isMessageLoading = MutableLiveData<Boolean>()
    val isMessageLoading: LiveData<Boolean> = _isMessageLoading

    fun setInitialMessage() {
        val openingMessage = "Selamat datang di Nusabasa, Siap Belajar, Kan?"
        val guidingMessage = "Coba Klik tombol dibawah untuk mulai ngobrol dengan aku!"

        val currentList = _messageList.value.orEmpty().toMutableList()
        currentList.addAll(
            listOf(
                Message(MessageType.RECEIVED_MESSAGE, openingMessage),
                Message(MessageType.RECEIVED_MESSAGE, guidingMessage),
                Message(MessageType.SUGGESTION_MESSAGE, "message")
            )
        )
        _messageList.value = currentList
    }


    fun sendMessage(message: String) {
        val currentList = _messageList.value.orEmpty().toMutableList()
        currentList.add(Message(MessageType.SENT_MESSAGE, message))
        _messageList.value = currentList

        receivedMessage(message)
    }

    private fun receivedMessage(message: String) {

        _isMessageLoading.value = true

        val currentList = _messageList.value.orEmpty().toMutableList()
        currentList.add(Message(MessageType.RECEIVED_MESSAGE, ""))
        _messageList.value = currentList

        val token = authRepository.getToken()

        viewModelScope.launch {
            val result = chatBotRepository.sendMessage(message, token!!)

            if (result is Result.Success){
                val updatedList = _messageList.value.orEmpty().toMutableList()

                val indexOfEmptyMessage = updatedList.indexOfFirst { it.messageText.isEmpty() }
                if (indexOfEmptyMessage != -1) {
                    updatedList[indexOfEmptyMessage] = Message(
                        result.data.type,
                        result.data.messageText
                    )
                    _messageList.value = updatedList
                }

                _isMessageLoading.value = false

            } else if (result is Result.Error) {

                val updatedList = _messageList.value.orEmpty().toMutableList()

                val indexOfEmptyMessage = updatedList.indexOfFirst { it.messageText.isEmpty() }
                if (indexOfEmptyMessage != -1) {
                    updatedList[indexOfEmptyMessage] = Message(
                        MessageType.RECEIVED_MESSAGE,
                        result.exception
                    )
                    _messageList.value = updatedList
                }

                _isMessageLoading.value = false
            }

        }
    }

}