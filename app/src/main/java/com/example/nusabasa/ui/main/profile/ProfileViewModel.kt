package com.example.nusabasa.ui.main.profile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nusabasa.data.api.repository.AuthRepository

class ProfileViewModel(private val authRepository: AuthRepository): ViewModel() {

    private val selectedImageUriLiveData = MutableLiveData<Uri?>()

    private val _profilePictureUrl = MutableLiveData<String>()
    val profilePictureUrl:LiveData<String> = _profilePictureUrl

    fun getProfilePicture(){
        _profilePictureUrl.value = "https://picsum.photos/200"
    }

    fun getUsername(): String {
        return authRepository.getUsername().toString()
    }

    fun getEmail(): String {
        return authRepository.getEmail().toString()
    }

    fun logout(){
        authRepository.logout()
    }

    fun setSelectedImageUri(uri: Uri?) {
        selectedImageUriLiveData.value = uri
    }

    fun getSelectedImageUri(): Uri? {
        return selectedImageUriLiveData.value
    }
}