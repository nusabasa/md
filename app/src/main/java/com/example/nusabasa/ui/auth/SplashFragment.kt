package com.example.nusabasa.ui.auth

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.nusabasa.R
import com.example.nusabasa.databinding.FragmentSplashBinding
import com.example.nusabasa.ui.main.MainActivity
import com.google.android.gms.auth.api.identity.SaveAccountLinkingTokenRequest.EXTRA_TOKEN

class SplashFragment : Fragment() {
    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    private val splashViewModel by viewModels<SplashViewModel> {
        AuthViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()

        val navOptions = NavOptions.Builder()
            .setPopUpTo(R.id.splashFragment, true)
            .build()

        val token = splashViewModel.getToken()

        Handler().postDelayed({
            if (token != null) {
                val intent = Intent(requireContext(), MainActivity::class.java)
                intent.putExtra(EXTRA_TOKEN, token)
                startActivity(intent)
                requireActivity().finish()
            } else {
                navController.navigate(R.id.action_splashFragment_to_loginFragment, null, navOptions)
            }
        }, 2000)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}