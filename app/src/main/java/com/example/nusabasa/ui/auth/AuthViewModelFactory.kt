package com.example.nusabasa.ui.auth

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nusabasa.data.di.Injection
import com.example.nusabasa.ui.auth.login.LoginViewModel
import com.example.nusabasa.ui.auth.register.RegisterViewModel

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class AuthViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                Injection.provideAuthRepository(context)
            ) as T
        }

        if (modelClass.isAssignableFrom(RegisterViewModel::class.java)){
            return RegisterViewModel(
                Injection.provideAuthRepository(context)
            ) as T
        }

        if (modelClass.isAssignableFrom(SplashViewModel::class.java)){
            return SplashViewModel(
                Injection.provideAuthRepository(context)
            ) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}