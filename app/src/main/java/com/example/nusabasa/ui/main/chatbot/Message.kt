package com.example.nusabasa.ui.main.chatbot

data class Message(
    val type: MessageType,
    val messageText: String
)
