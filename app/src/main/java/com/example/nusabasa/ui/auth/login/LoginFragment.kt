package com.example.nusabasa.ui.auth.login

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.nusabasa.R
import com.example.nusabasa.databinding.FragmentLoginBinding
import com.example.nusabasa.ui.auth.AuthViewModelFactory
import com.example.nusabasa.ui.auth.validator.ResultListener
import com.example.nusabasa.ui.misc.LoadingOverlay
import com.google.android.material.textfield.TextInputEditText

class LoginFragment : Fragment() {
    private var resultListener: ResultListener? = null

    private var _binding: FragmentLoginBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val loadingDialogFragment by lazy { LoadingOverlay() }
    private val loginViewModel by viewModels<LoginViewModel>{
        AuthViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun showLoader(){
        //Show Loader
        if (!loadingDialogFragment.isAdded){
            loadingDialogFragment.show(parentFragmentManager, "loader_login")
        }
    }

    private fun hideLoader(){
        //Hide Loader
        if (loadingDialogFragment.isAdded) {
            loadingDialogFragment.dismissAllowingStateLoss()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val email = binding.inputUsername
        val password = binding.inputPassword
        val login = binding.btnLogin

        binding.textViewNotHaveAccount.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }


        loginViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            val loadingState = it ?: return@Observer

            when (loadingState){
                true -> showLoader()
                false -> hideLoader()
            }
        })

        loginViewModel.loginFormState.observe(viewLifecycleOwner, Observer {
            //make sure observer handles once
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.emailError != null) {
                email.error = getString(loginState.emailError)
            }
        })

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {event ->
            //make sure observer handles once
            val loginResult = event.getContentIfNotHandled() ?: return@Observer

            resultListener?.onResult(LOGIN_RESULT_OK)

            if (loginResult.failedLogin != null) {
                Toast.makeText(activity,
                    loginResult.failedLogin.message,
                    Toast.LENGTH_SHORT
                ).show()
            }
            if (loginResult.successLogin != null) {
                val bundle = bundleOf(EXTRA_BUNDLE to loginResult.successLogin.token)
                findNavController().navigate(
                    R.id.action_loginFragment_to_mainActivity,
                    bundle
                )
                activity?.finish()
            }
        })

        email.afterTextChanged {
            loginViewModel.loginDataChanged(
                email.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    email.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId){
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            email.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                resultListener?.onResult(LOGIN_RESULT_ONGOING)

                loginViewModel.login(
                    email.text.toString(),
                    password.text.toString()
                )
            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ResultListener) {
            resultListener = context
        }
    }

    companion object{
        const val EXTRA_BUNDLE = "extra_bundle"
        const val LOGIN_RESULT_OK = 200
        const val LOGIN_RESULT_ONGOING = 220
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun TextInputEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}