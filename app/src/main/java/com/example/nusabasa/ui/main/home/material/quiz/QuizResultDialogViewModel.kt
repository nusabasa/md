package com.example.nusabasa.ui.main.home.material.quiz

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.api.repository.QuizRepository
import kotlinx.coroutines.launch

class QuizResultDialogViewModel(
    private val quizRepository: QuizRepository,
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _isSubmitSuccess = MutableLiveData<Boolean>()
    val isSubmitSuccess : LiveData<Boolean> = _isSubmitSuccess

    fun submitXP(point : Int){

        val jwtToken = "Bearer ${authRepository.getToken()}"

        viewModelScope.launch {
            val result = quizRepository.submitQuiz(point, jwtToken)

            _isSubmitSuccess.value = result is Result.Success
        }
    }
}