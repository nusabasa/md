package com.example.nusabasa.ui.main.home

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.CircleCropTransformation
import com.example.nusabasa.R
import com.example.nusabasa.data.api.response.MaterialCategoryResponseItem
import com.example.nusabasa.databinding.FragmentHomeBinding
import com.example.nusabasa.ui.main.MainViewModelFactory
import com.example.nusabasa.ui.main.home.composables.TriviaComponent
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton

class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var prevScrollY = 0
    private var isFabVisible = true

    private val homeViewModel by viewModels<HomeViewModel> {
        MainViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Top Greeting Card
        setupGreetingCard()

        setupTriviaComposable()

        //Material List
        setupMaterialList()

        val homeScrollView = binding.homeScrollView

        val textViewAvailableMaterial = binding.textviewAvailableMaterials

        binding.beginStudyMain.btnScrollToMaterial.setOnClickListener {
            homeScrollView.smoothScrollTo(0, (textViewAvailableMaterial.top - 70), 1275)
        }

        homeViewModel.getMaterialCategory()

        view.post {
            val fab = requireActivity().findViewById<ExtendedFloatingActionButton>(R.id.fab_chatbot)

            homeScrollView.setOnScrollChangeListener { _, _, scrollY, _, _ ->
                handleScroll(scrollY, homeScrollView, fab)
            }
        }

    }

    private fun handleScroll(
        scrollY: Int,
        homeScrollView: NestedScrollView,
        fab: ExtendedFloatingActionButton
    ) {
        val scrollThreshold = 8 // Adjust this threshold value as needed

        if (scrollY > prevScrollY && scrollY > scrollThreshold && isFabVisible) {
            // User is scrolling down, and FAB is visible
            animateFabCollapse(fab)
        } else if (scrollY < prevScrollY && scrollY < (homeScrollView.getChildAt(0).height - homeScrollView.height - scrollThreshold) && !isFabVisible) {
            // User is scrolling up, and FAB is not visible
            animateFabExpand(fab)
        }

        prevScrollY = scrollY
    }

    private fun animateFabCollapse(fab: ExtendedFloatingActionButton) {
        fab.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fab_collapse))
        fab.visibility = View.INVISIBLE
        isFabVisible = false
    }

    private fun animateFabExpand(fab: ExtendedFloatingActionButton) {
        fab.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fab_expand))
        fab.visibility = View.VISIBLE
        isFabVisible = true
    }


    private fun setupGreetingCard() {
        val profileImageUri = getProfileImageFromSharedPreferences()

        binding.topGreetingMain.ivProfileGreeting.load(profileImageUri) {
            transformations(CircleCropTransformation())
            crossfade(true)
            placeholder(R.drawable.default_profile_image)
        }

        // Jika gambar profil tidak tersedia, gunakan foto default
        if (profileImageUri == null) {
            binding.topGreetingMain.ivProfileGreeting.load("https://picsum.photos/200") {
                transformations(CircleCropTransformation())
                crossfade(true)
                placeholder(R.drawable.default_profile_image)
            }
        }

        binding.topGreetingMain.tvNameGreeting.text = homeViewModel.getUsername()
    }


    private fun getProfileImageFromSharedPreferences(): Uri? {
        val sharedPreferences = requireContext().getSharedPreferences("Profile", Context.MODE_PRIVATE)
        val savedUriString = sharedPreferences.getString("selectedImageUri", null)
        return if (savedUriString != null) Uri.parse(savedUriString) else null
    }


    private fun setupTriviaComposable() {
        //Trivia Composable
        binding.triviaComposableRecyclerview.setContent {
            TriviaComponent()
        }
    }

    private fun setupMaterialList() {

        homeViewModel.categoryItems.observe(viewLifecycleOwner, Observer {
            val materialCategoryItems = it ?: return@Observer

            Log.d(TAG, "ITEMS : $materialCategoryItems")

            val adapter = HomeMaterialAdapter((materialCategoryItems))

            val homeRecyclerView = binding.rvMaterialList
            homeRecyclerView.adapter = adapter
            homeRecyclerView.layoutManager = LinearLayoutManager(requireContext())

            adapter.setOnItemClickCallback(object : HomeMaterialAdapter.OnItemClickCallback{
                override fun onItemClicked(data: MaterialCategoryResponseItem) {
                    val args = Bundle().apply {
                        putInt(CATEGORY_ID, data.id!!)
                        putString(CATEGORY_NAME, data.name)
                    }

                    findNavController().navigate(
                        R.id.action_navigation_home_to_materialFragment2,
                        args,
                    )
                }
            })
        })
    }

    companion object {
        const val CATEGORY_ID = "category_id"
        const val CATEGORY_NAME = "category_name"
        private const val TAG = "HomeFragment"
    }
}