package com.example.nusabasa.ui.main.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.compose.foundation.shape.CircleShape
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import coil.load
import coil.transform.CircleCropTransformation
import com.example.nusabasa.R
import com.example.nusabasa.databinding.FragmentProfileBinding
import com.example.nusabasa.ui.auth.AuthActivity
import com.example.nusabasa.ui.main.MainViewModelFactory
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.ShapeAppearanceModel
import java.io.File
import java.io.FileOutputStream

class ProfileFragment : Fragment() {

    private val binding: FragmentProfileBinding by lazy {
        FragmentProfileBinding.inflate(layoutInflater)
    }
    private val profileViewModel by viewModels<ProfileViewModel>{
        MainViewModelFactory(requireContext())
    }

    private var selectedImageUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val logout = binding.logoutButton

        val sharedPreferences = requireContext().getSharedPreferences("Profile", Context.MODE_PRIVATE)
        val savedUriString = sharedPreferences.getString("selectedImageUri", null)
        savedUriString?.let {
            selectedImageUri = Uri.parse(it)
            binding.profileImageView.setImageURI(selectedImageUri)
        }
        val editor = sharedPreferences.edit()
        editor.putString("selectedImageUri", selectedImageUri?.toString())
        editor.apply()

        // Mengatur bentuk lingkaran pada ImageView
        val shapeAppearanceModel = ShapeAppearanceModel.builder()
            .setAllCorners(CornerFamily.ROUNDED, 50f)
            .build()

        binding.profileImageView.shapeAppearanceModel = shapeAppearanceModel

        profileViewModel.getProfilePicture()

        profileViewModel.profilePictureUrl.observe(viewLifecycleOwner) { imageUrl ->
            binding.profileImageView.load(imageUrl) {
                crossfade(true)
                placeholder(R.drawable.default_profile_image)
                transformations(CircleCropTransformation())
            }
        }


        logout.setOnClickListener {

            // Menghapus foto yang disimpan di Shared Preferences
            val sharedPreferences = requireContext().getSharedPreferences("Profile", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.remove("selectedImageUri")
            editor.apply()

            // Menghapus foto dari tampilan
            binding.profileImageView.setImageURI(null)

            profileViewModel.logout()

            val intent = Intent(requireContext(), AuthActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            requireActivity().finish()
        }

        binding.nameTextView.text = profileViewModel.getUsername()
        binding.emailTextView.text = profileViewModel.getEmail()


        val changePhotoButton = binding.changePhotoButton
        changePhotoButton.setOnClickListener {
            showConsentDialog()
        }

    }

    private fun showConsentDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Maaf... 😔")
            .setMessage("Fitur ini masih dalam pengembangan, sehingga masih ada kemungkinan menyebabkan error. Kamu yakin ingin mencobanya?")
            .setPositiveButton("Ya, Saya Sadar Konsekuensinya") { _, _ ->
                openGallery()
            }
            .setNegativeButton("Tidak") { dialog, _ ->
                // User clicked "No" button, dismiss the dialog and finish the activity
                dialog.dismiss()
            }
            .setCancelable(false) // Prevent dialog dismissal when clicking outside or pressing back button
            .setIcon(R.drawable.ic_baseline_warning_24)

        val dialog = builder.create()
        dialog.show()
    }


    private fun openGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            selectedImageUri = data.data
            // Check to make sure that the selected image URI is not null
            if (selectedImageUri != null) {
                // Set the image view to the selected image
                binding.profileImageView.setImageURI(selectedImageUri)

                // Save the selected image URI in Shared Preferences
                val sharedPreferences = requireContext().getSharedPreferences("Profile", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("selectedImageUri", selectedImageUri.toString())
                editor.apply()

                // Update the selected image URI in the view model
                profileViewModel.setSelectedImageUri(selectedImageUri)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        // Load the selected image URI from Shared Preferences

        // Check to make sure that the selectedImageUri variable is not null
        if (selectedImageUri != null) {
            // Set the image view to the selected image
            binding.profileImageView.setImageURI(selectedImageUri)
        }

    }

    companion object {
        private const val TAG = "ProfileFragment"
        private const val GALLERY_REQUEST_CODE = 100
    }
}