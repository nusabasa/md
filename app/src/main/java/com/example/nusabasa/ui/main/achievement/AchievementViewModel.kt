package com.example.nusabasa.ui.main.achievement

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.api.repository.LeaderboardRepository
import com.example.nusabasa.data.api.response.LeaderboardResponseItem
import kotlinx.coroutines.launch

class AchievementViewModel(
    private val leaderboardRepository: LeaderboardRepository,
    private val authRepository: AuthRepository
): ViewModel() {

    private val token = authRepository.getToken()

    private val _leaderboard = MutableLiveData<List<LeaderboardResponseItem>>()
    val leaderboard: LiveData<List<LeaderboardResponseItem>> = _leaderboard

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun getLeaderboard() {

        _isLoading.value = true

        viewModelScope.launch {
            if (token != null) {

                val result = leaderboardRepository.getLeaderboard(token)

                if (result is Result.Success){
                    _leaderboard.value = result.data.leaderboardResponse as List<LeaderboardResponseItem>
                    _isLoading.value = false
                    Log.d(TAG, "SUCCESS : ${_leaderboard.value} ")
                } else if (result is Result.Error){
                    _isLoading.value = false
                }
            }
        }
    }

    companion object {
        private const val TAG = "AchievementViewModel"
    }

}