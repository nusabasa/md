package com.example.nusabasa.ui.main.achievement

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nusabasa.data.api.response.LeaderboardResponseItem
import com.example.nusabasa.databinding.ItemLeaderboardBinding


class AchievementAdapter(
    private val achievementList: List<LeaderboardResponseItem>
) : RecyclerView.Adapter<AchievementAdapter.AchievementViewHolder>() {

    inner class AchievementViewHolder(private val binding: ItemLeaderboardBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(leaderboard: LeaderboardResponseItem, position: Int) {
            // Bind data achievement ke elemen-elemen dalam layout item_leaderboard.xml
            binding.rankTextView.text = position.plus(1).toString()
            binding.nameTextView.text = leaderboard.username.toString()
            binding.xpTextView.text = leaderboard.userpoint.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AchievementViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemLeaderboardBinding.inflate(inflater, parent, false)
        return AchievementViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AchievementViewHolder, position: Int) {
        val achievement = achievementList[position]
        holder.bind(achievement, position)
    }

    override fun getItemCount(): Int = achievementList.size
}
