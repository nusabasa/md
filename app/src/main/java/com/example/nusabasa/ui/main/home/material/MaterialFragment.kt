package com.example.nusabasa.ui.main.home.material

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.nusabasa.R
import com.example.nusabasa.databinding.FragmentMaterialBinding
import com.example.nusabasa.ui.main.home.HomeFragment.Companion.CATEGORY_ID
import com.example.nusabasa.ui.main.home.HomeFragment.Companion.CATEGORY_NAME
import com.example.nusabasa.ui.main.home.material.quiz.QuizActivity
import com.example.nusabasa.ui.main.home.material.selected_material.SelectedMaterialFragment


data class MaterialCategory(
    val id: Int,
    val name: String
)
class MaterialFragment : Fragment() {

    private var _binding: FragmentMaterialBinding? = null
    private val binding get() = _binding!!

    var progress: Int = 0

    private var progressIndex: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMaterialBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHeaders()

        val selectedCategoryId = arguments?.getInt(CATEGORY_ID)
        val selectedCategoryName = arguments?.getString(CATEGORY_NAME)

        binding.textViewCategoryName.text = selectedCategoryName

        Log.d(TAG, "NAVIGATOR : $selectedCategoryId")

        setupMaterialNavigator(selectedCategoryName,selectedCategoryId )

        binding.beginnerLevelMenu.quizButton.setOnClickListener {
            val intent = Intent(requireContext(), QuizActivity::class.java)
            intent.putExtra(CATEGORY_ID, selectedCategoryId)
            intent.putExtra(LEVEL_ID, 1)
            startActivity(intent)
        }

        binding.intermediateLevelMenu.quizButton.setOnClickListener {
            val intent = Intent(requireContext(), QuizActivity::class.java)
            intent.putExtra(CATEGORY_ID, selectedCategoryId)
            intent.putExtra(LEVEL_ID, 2)
            startActivity(intent)
        }

        binding.advancedLevelMenu.quizButton.setOnClickListener {
            val intent = Intent(requireContext(), QuizActivity::class.java)
            intent.putExtra(CATEGORY_ID, selectedCategoryId)
            intent.putExtra(LEVEL_ID, 3)
            startActivity(intent)
        }
    }

    private fun setupMaterialNavigator(selectedCategoryName: String?, selectedCategoryId: Int?) {
        binding.beginnerLevelMenu.materiButton.setOnClickListener {
            navigateToSelectedMaterial("easy",
                selectedCategoryName.toString(), 1, selectedCategoryId!!)
        }

        // Intermediate Level
        binding.intermediateLevelMenu.materiButton.setOnClickListener {
            navigateToSelectedMaterial("medium",
                selectedCategoryName.toString(), 2, selectedCategoryId!!)
        }

        // Advanced Level
        binding.advancedLevelMenu.materiButton.setOnClickListener {
            navigateToSelectedMaterial("hard",
                selectedCategoryName.toString(), 3, selectedCategoryId!!)
        }
    }

    private fun navigateToSelectedMaterial(categoryDifficulty: String, category: String, levelId : Int, categoryId: Int) {
        val bundle = Bundle().apply {
            putString(MATERIAL_CATEGORY_DIFFICULTY_NAME, categoryDifficulty)
            putString(CATEGORY_NAME, category)
            putInt(LEVEL_ID, levelId)
            putInt(CATEGORY_ID, categoryId)
            putInt(SelectedMaterialFragment.PROGRESS_INDEX, progressIndex)
        }

        findNavController().navigate(
            R.id.action_materialFragment2_to_selectedMaterialFragment,
            bundle
        )
    }

    private fun setupHeaders() {
        // Beginner Level
        binding.beginnerHeader.levelTextView.text = "Level Mudah"

        // Intermediate Level
        binding.intermediateHeader.levelTextView.text = "Level Sedang"

        // Advanced Level
        binding.advancedHeader.levelTextView.text = "Level Sulit"
    }

    override fun onResume() {
        super.onResume()
        // Dapatkan nilai progressIndex terbaru dari Shared Preferences atau sumber lainnya
        // Misalnya, disini kita menggunakan nilai yang disimpan dalam variabel progressIndex

        val args = arguments
        if (args != null && args.containsKey(SelectedMaterialFragment.PROGRESS_INDEX)) {
            progressIndex = args.getInt(SelectedMaterialFragment.PROGRESS_INDEX)
        }

        val progress = progressIndex * 20 // Ubah sesuai dengan logika peningkatan progress

//        // Update progress bar
//        binding.beginnerHeader.progressBar.progress = progress
//        binding.intermediateHeader.progressBar.progress = progress
//        binding.advancedHeader.progressBar.progress = progress
    }
    companion object {
        private const val TAG = "MaterialFragment"
        const val MATERIAL_CATEGORY_DIFFICULTY_NAME = "material_category_difficulty_name"
        const val LEVEL_ID = "level_id"
        const val PROGRESS_INDEX = "progress_index"
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
