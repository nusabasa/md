package com.example.nusabasa.ui.main.chatbot

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import com.example.nusabasa.R
import com.example.nusabasa.databinding.ActivityChatBotBinding
import com.example.nusabasa.ui.main.MainViewModelFactory

class ChatBotActivity : AppCompatActivity(),MessageAdapter.OnSecondItemClickCallback {

    private val binding by lazy {
        ActivityChatBotBinding.inflate(layoutInflater)
    }

    private val chatBotViewModel by viewModels<ChatBotViewModel> {
        MainViewModelFactory(this@ChatBotActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        chatBotViewModel.setInitialMessage()

        chatBotViewModel.isMessageLoading.observe(this){isLoading ->
            binding.buttonSendMessage.isClickable = !isLoading
            binding.buttonSendMessage.isEnabled = !isLoading

            val backgroundDrawable = if (isLoading) {
                R.drawable.background_chat_send_button_loading
            } else {
                R.drawable.background_chat_send_button
            }
            binding.buttonSendMessage.setBackgroundResource(backgroundDrawable)
        }

        binding.buttonSendMessage.setOnClickListener {
            Toast.makeText(
                this,
                "Sent",
                Toast.LENGTH_SHORT
            ).show()

            chatBotViewModel.sendMessage(binding.textMessageInput.text.toString())

            binding.textMessageInput.text = null
        }

        val messageAdapter = MessageAdapter(emptyList())
        messageAdapter.setCallback(this)

        chatBotViewModel.messageList.observe(this) { messages ->
            messageAdapter.updateList(messages)
            if (binding.textMessageInput.hasFocus() && messageAdapter.itemCount > 0) {
                // Scroll to the bottom if the input field has focus and messageAdapter is not empty
                binding.chatRecyclerView.scrollToPosition(messageAdapter.itemCount - 1)
            }
        }

        binding.chatRecyclerView.adapter = messageAdapter

        binding.textMessageInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus && messageAdapter.itemCount > 0) {
                // Scroll to the bottom when the input field gains focus and messageAdapter is not empty
                binding.chatRecyclerView.scrollToPosition(messageAdapter.itemCount - 1)
            }
        }
    }

    override fun onBackPressed() {
        showExitConfirmationDialog()
    }

    private fun showExitConfirmationDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Konfirmasi Keluar Chatbot")
            .setMessage("Apa kamu yakin ingin keluar dari Chatbot? Semua Percakapan akan hilang.")
            .setPositiveButton("Ya") { _, _ ->
                // User clicked "Yes" button, finish the activity
                finish()
            }
            .setNegativeButton("Tidak") { dialog, _ ->
                // User clicked "No" button, dismiss the dialog
                dialog.dismiss()
            }
            .setCancelable(false) // Prevent dialog dismissal when clicking outside or pressing back button
            .setIcon(R.drawable.ic_baseline_warning_24)

        val dialog = builder.create()
        dialog.show()
    }

    override fun onSecondItemClicked(message: String) {
        chatBotViewModel.sendMessage(message)
    }
}