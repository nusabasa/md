package com.example.nusabasa.ui.main.home.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils
import coil.compose.AsyncImage
import com.example.nusabasa.ui.main.ui.theme.senFont
import com.example.nusabasa.utils.generateFunfactList

@Composable
fun TriviaComponent(
    modifier: Modifier = Modifier
) {
    val dummyList = remember { generateFunfactList() }
    val expandedState = remember { mutableStateOf(-1) }

    Column {
        Box(
            modifier = Modifier.padding(start = 16.dp)
        ) {
            Text(
                text = "Fakta Menarik 🤔",
                fontSize = 24.sp,
                fontFamily = senFont,
                fontWeight = FontWeight.Bold,
            )
        }
        LazyRow(
            modifier = modifier,
            verticalAlignment = Alignment.CenterVertically
        ){
            items(dummyList, key = { it.id }) { item ->
                TriviaItemCard(
                    triviaTitle = item.placeholderTitle,
                    triviaPhotoUrl = item.placeholderPhotoUrl,
                    triviaContent = item.placeholderContent,
                    modifier = modifier,
                    randomColor = generateRandomColor(),
                    isExpanded = expandedState.value == item.id,
                    onExpandClicked = { expanded ->
                        if (expanded) {
                            expandedState.value = item.id
                        } else {
                            expandedState.value = -1
                        }
                    }
                )
            }
        }
    }
}

@Composable
fun TriviaItemCard(
    triviaTitle: String,
    triviaPhotoUrl: String,
    triviaContent: String,
    modifier: Modifier = Modifier,
    randomColor: Color,
    isExpanded: Boolean,
    onExpandClicked: (Boolean) -> Unit
) {
    val darkerColor = darkenColor(randomColor)

    Card(
        modifier = modifier
            .padding(16.dp)
            .width(125.dp),
        elevation = 4.dp,
        backgroundColor = randomColor,
        shape = RoundedCornerShape(12.dp),
        border = BorderStroke(2.dp, color = darkerColor)
    ) {
        Column(
            modifier = Modifier.padding(8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = triviaTitle,
                style = MaterialTheme.typography.h6,
                fontFamily = senFont,
                fontWeight = FontWeight.SemiBold,
                fontSize = 16.sp,
                textAlign = TextAlign.Center
            )
            AsyncImage(
                model = triviaPhotoUrl,
                contentDescription = "PlaceholderData Description",
                contentScale = ContentScale.Crop,
                modifier = Modifier.size(100.dp)
            )
            AnimatedVisibility(
                visible = isExpanded,
                enter = fadeIn() + expandVertically(),
                exit = fadeOut() + shrinkVertically()
            ) {
                Text(
                    text = triviaContent,
                    style = MaterialTheme.typography.caption,
                    textAlign = TextAlign.Center,
                    fontFamily = senFont,
                    fontSize = 12.sp
                )
            }
            IconButton(
                onClick = { onExpandClicked(!isExpanded) },
                modifier = Modifier.padding(top = 8.dp)
            ) {
                Icon(
                    imageVector = if (isExpanded) Icons.Default.KeyboardArrowUp else Icons.Default.KeyboardArrowDown,
                    contentDescription = if (isExpanded) "Collapse" else "Expand"
                )
            }
        }
    }
}



fun darkenColor(color: Color, amount: Float = 0.3f): Color {
    val colorInt = color.toArgb()
    val darkenedColorInt = ColorUtils.blendARGB(colorInt, Color.Black.toArgb(), amount)
    return Color(darkenedColorInt)
}

@Composable
fun generateRandomColor(): Color {
    val randomColorHex = listOf(
        "#FEFAEE",
        "#C6C5FF",
        "#FFC1D3",
        "#B0FADB",
        "#C3F8FF",
    ).random()
    return Color(android.graphics.Color.parseColor(randomColorHex))
}


@Preview
@Composable
fun PreviewTriviaScreen(){
    TriviaComponent()
}

//@Preview
//@Composable
//fun PreviewTriviaItemCard(){
//    val randomColor = generateRandomColor()
//    TriviaItemCard(
//        triviaTitle = "Title",
//        triviaPhotoUrl = "https://picsum.photos/200",
//        triviaContent = "Content",
//        randomColor = randomColor
//    )
//}