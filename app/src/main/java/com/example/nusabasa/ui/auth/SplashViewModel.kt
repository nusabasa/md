package com.example.nusabasa.ui.auth

import androidx.lifecycle.ViewModel
import com.example.nusabasa.data.api.repository.AuthRepository

class SplashViewModel(private val authRepository: AuthRepository): ViewModel() {

    fun getToken() : String?{
        return authRepository.getToken()
    }
}