package com.example.nusabasa.ui.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.R
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.Result
import com.example.nusabasa.ui.auth.validator.AuthFormState
import com.example.nusabasa.ui.auth.validator.AuthResult
import com.example.nusabasa.utils.Event
import com.example.nusabasa.utils.isPasswordValid
import kotlinx.coroutines.launch

class LoginViewModel(private val authRepository: AuthRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<AuthFormState>()
    val loginFormState: LiveData<AuthFormState> = _loginForm

    private val _loginResult = MutableLiveData<Event<AuthResult>>()
    val loginResult: LiveData<Event<AuthResult>> = _loginResult

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun login(emailOrUsername: String, password: String) {

        _isLoading.value = true
        // can be launched in a separate asynchronous job
        viewModelScope.launch {
            val result = authRepository.login(emailOrUsername, password)

            if (result is Result.Success) {
                _loginResult.value = Event(
                    AuthResult(successLogin = LoggedInUserView(
                        message = result.data.name.toString(),
                        token = result.data.token.toString()
                    ))
                )

                _isLoading.value = false
            } else if (result is Result.Error) {
                _loginResult.value = Event(
                    AuthResult(failedLogin = LoggedInUserView(
                        message = result.exception,
                        token = null
                    ))
                )

                _isLoading.value = false
            }
        }
    }

    fun loginDataChanged(emailOrUsername: String, password: String) {
        if (!isPasswordValid(password)) {
            _loginForm.value = AuthFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = AuthFormState(isDataValid = true)
        }
    }
}