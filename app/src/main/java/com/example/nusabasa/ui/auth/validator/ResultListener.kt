package com.example.nusabasa.ui.auth.validator

interface ResultListener {
    fun onResult(resultCode: Int)
}