package com.example.nusabasa.ui.auth.register

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.R
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.Result
import com.example.nusabasa.ui.auth.validator.AuthFormState
import com.example.nusabasa.ui.auth.validator.AuthResult
import com.example.nusabasa.utils.Event
import com.example.nusabasa.utils.isEmailValid
import com.example.nusabasa.utils.isNameValid
import com.example.nusabasa.utils.isPasswordValid
import kotlinx.coroutines.launch

class RegisterViewModel(private val authRepository: AuthRepository) : ViewModel() {

    private val _registerForm = MutableLiveData<AuthFormState>()
    val registerFormState: LiveData<AuthFormState> = _registerForm

    private val _registerResult = MutableLiveData<Event<AuthResult>>()
    val registerResult: LiveData<Event<AuthResult>> = _registerResult

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading : LiveData<Boolean> = _isLoading

    fun register(name: String, email: String, password: String) {

        _isLoading.value = true

        viewModelScope.launch {
            val result = authRepository.register(name, email, password)

            if (result is Result.Success){
                val successResult = AuthResult(successRegister = RegisteredUserView(message = result.data.message))
                _registerResult.value = Event(successResult)
                Log.d(TAG, "register function called : ${registerResult.value}")

                _isLoading.value = false

            }else if (result is Result.Error) {
                val errorResult = AuthResult(failedRegister = RegisteredUserView(message = result.exception))
                _registerResult.value = Event(errorResult)
                Log.e(TAG, "register function called : ${registerResult.value}")

                _isLoading.value = false
            }
        }
    }

    fun registerDataChanged(name: String, email: String, password: String) {
        if (!isNameValid(name)){
            _registerForm.value = AuthFormState(nameError = R.string.invalid_name)
        } else if (!isEmailValid(email)){
            _registerForm.value = AuthFormState(emailError = R.string.invalid_email)
        } else if (!isPasswordValid(password)){
            _registerForm.value = AuthFormState(passwordError = R.string.invalid_password)
        } else {
            _registerForm.value = AuthFormState(isDataValid = true)
        }
    }

    companion object{
        private const val TAG = "RegisterViewModel"
    }

}