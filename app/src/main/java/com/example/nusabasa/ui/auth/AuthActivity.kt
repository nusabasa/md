package com.example.nusabasa.ui.auth

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.nusabasa.R
import com.example.nusabasa.databinding.ActivityAuthBinding
import com.example.nusabasa.ui.misc.NetworkConnectivityListener

class AuthActivity : AppCompatActivity(), NetworkConnectivityListener.NetworkListener {
    private lateinit var binding: ActivityAuthBinding
    private lateinit var networkConnectivityListener: NetworkConnectivityListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        networkConnectivityListener = NetworkConnectivityListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterNetworkReceiver()
    }

    override fun onResume() {
        super.onResume()
        registerNetworkReceiver()
    }

    override fun onPause() {
        super.onPause()
        unregisterNetworkReceiver()
    }

    private fun registerNetworkReceiver() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(networkConnectivityListener, intentFilter)
    }

    private fun unregisterNetworkReceiver() {
        try {
            unregisterReceiver(networkConnectivityListener)
        } catch (e: IllegalArgumentException) {
            // Receiver was not registered, ignore the exception
        }
    }


    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            showNetworkDialog()
        } else {
            // Connection is restored, perform necessary actions here
        }
    }

    private fun showNetworkDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Maaf... 😔")
            .setMessage("Nusabasa tidak bisa berjalan dengan optimal tanpa koneksi internet. Periksa kembali jaringan kamu")
            .setPositiveButton("Coba Lagi") { dialog, _ ->
                // Retry logic
                dialog.dismiss()
                retryConnection()
            }
            .setNegativeButton("Keluar Aplikasi") { dialog, _ ->
                // User clicked "No" button, dismiss the dialog and finish the activity
                dialog.dismiss()
                finish()
            }
            .setCancelable(false) // Prevent dialog dismissal when clicking outside or pressing back button
            .setIcon(R.drawable.ic_wifi_low)

        val dialog = builder.create()
        dialog.show()
    }

    private fun retryConnection() {
        // Perform the necessary actions to check the connection again
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if (isConnected) {
            // Connection is restored, perform necessary actions here
        } else {
            showNetworkDialog()
        }
    }

}