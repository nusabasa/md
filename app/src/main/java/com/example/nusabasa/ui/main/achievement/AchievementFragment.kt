package com.example.nusabasa.ui.main.achievement

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nusabasa.R
import com.example.nusabasa.databinding.FragmentAchievementBinding
import com.example.nusabasa.ui.main.MainViewModelFactory
import com.example.nusabasa.ui.misc.LoadingOverlay


class AchievementFragment : Fragment() {
    private var _binding: FragmentAchievementBinding? = null
    private val binding get() = _binding!!

    private val achievementViewModel by viewModels<AchievementViewModel> {
        MainViewModelFactory(requireContext())
    }

    private val loadingDialogFragment by lazy { LoadingOverlay() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAchievementBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        achievementViewModel.getLeaderboard()
        setupAchievementList()

        achievementViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            val loadingState = it ?: return@Observer

            when (loadingState){
                true -> showLoader()
                false -> hideLoader()
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupAchievementList() {
        achievementViewModel.leaderboard.observe(viewLifecycleOwner, Observer {
            val leaderboard = it ?: return@Observer

            val adapter = AchievementAdapter(leaderboard)
            binding.leaderboardRecyclerView.adapter = adapter
            binding.leaderboardRecyclerView.layoutManager = LinearLayoutManager(
                requireContext()
            )

            val itemDecoration = DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )

            binding.leaderboardRecyclerView.addItemDecoration(itemDecoration)
        })
    }

    private fun showLoader(){
        //Show Loader
        if (!loadingDialogFragment.isAdded){
            loadingDialogFragment
                .show(parentFragmentManager, "loader_achievement")
        }
    }

    private fun hideLoader(){
        //Hide Loader
        if (loadingDialogFragment.isAdded) {
            loadingDialogFragment.dismissAllowingStateLoss()
        }
    }
}

