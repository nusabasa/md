package com.example.nusabasa.ui.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nusabasa.data.Result
import com.example.nusabasa.data.api.repository.AuthRepository
import com.example.nusabasa.data.api.repository.MaterialRepository
import com.example.nusabasa.data.api.response.MaterialCategoryResponseItem
import kotlinx.coroutines.launch

class HomeViewModel(
    private val materialRepository: MaterialRepository,
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _categoryItems = MutableLiveData<List<MaterialCategoryResponseItem>>()
    val categoryItems : LiveData<List<MaterialCategoryResponseItem>> = _categoryItems

    fun getUsername(): String {
        return authRepository.getUsername().toString()
    }

    fun getMaterialCategory(){
        viewModelScope.launch {
            val result = materialRepository.getMaterialCategory()

            if (result is Result.Success){
                _categoryItems.value = result.data.materialCategoryResponse as List<MaterialCategoryResponseItem>
                Log.d(TAG, "SUCESS $result")
            } else if (result is Result.Error){
                Log.e(TAG, "ERROR $result")
            }
        }
    }

    companion object {
        private const val TAG = "HomeViewModel"
    }

}