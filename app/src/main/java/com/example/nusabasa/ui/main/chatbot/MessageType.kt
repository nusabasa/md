package com.example.nusabasa.ui.main.chatbot

enum class MessageType {
    SENT_MESSAGE,
    RECEIVED_MESSAGE,
    SUGGESTION_MESSAGE
}