package com.example.nusabasa.ui.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.nusabasa.R
import com.example.nusabasa.data.api.response.MaterialCategoryResponseItem
import com.example.nusabasa.databinding.ItemMaterialHomeBinding

class HomeMaterialAdapter(private val materialCategoryItemList: List<MaterialCategoryResponseItem>) :
    RecyclerView.Adapter<HomeMaterialAdapter.ViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback{
        fun onItemClicked(data: MaterialCategoryResponseItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMaterialHomeBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val placeholderData = materialCategoryItemList[position]
        holder.bind(placeholderData)
    }

    override fun getItemCount(): Int {
        return materialCategoryItemList.size
    }

    inner class ViewHolder(private val binding: ItemMaterialHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(materialCategoryItem: MaterialCategoryResponseItem) {
            binding.apply {
                textviewTitle.text = materialCategoryItem.name
                textviewDescription.text = materialCategoryItem.description
                circularImage.load("https://picsum.photos/200"){
                    transformations(CircleCropTransformation())
                    crossfade(true)
                    placeholder(R.drawable.default_profile_image)
                }
                button.setOnClickListener {
                    materialCategoryItem.let { data ->
                        onItemClickCallback.onItemClicked(data)
                    }
                }
            }
        }
    }
}
