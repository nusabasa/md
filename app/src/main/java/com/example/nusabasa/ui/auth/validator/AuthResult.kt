package com.example.nusabasa.ui.auth.validator

import com.example.nusabasa.ui.auth.login.LoggedInUserView
import com.example.nusabasa.ui.auth.register.RegisteredUserView

/**
 * Authentication result : success (user details) or error message.
 */
data class AuthResult(
    val successLogin: LoggedInUserView? = null,
    val successRegister: RegisteredUserView? = null,
    val failedLogin: LoggedInUserView? = null,
    val failedRegister: RegisteredUserView? = null
)